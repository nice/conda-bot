from conda_bot.app import app
from starlette.testclient import TestClient


def test_dummy_webhook():
    """Check that the bot can process a webhook request"""
    headers = {"x-gitlab-event": "Dummy Hook", "x-gitlab-token": "12345"}
    data = {"msg": "testing webhook request"}
    with TestClient(app) as client:
        response = client.post("/", headers=headers, json=data)
    assert response.status_code == 200
