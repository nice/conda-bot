import logging
import pytest
from gidgetlab_cli.models import Project
from conda_bot import tasks
from .utils import FakeGitLab


@pytest.mark.asyncio
async def test_create_recipe_merge_request_invalid_tag(caplog, mocker):
    mock_find_project = mocker.patch("conda_bot.tasks.api.find_project")
    gl = FakeGitLab()
    project = Project(
        name="foo", namespace="test-group", path_with_namespace="test-group/foo"
    )
    user_id = 42
    tag = "test_tag"
    with caplog.at_level(logging.INFO):
        await tasks.create_recipe_merge_request(gl, project, tag, user_id)
    mock_find_project.assert_not_awaited()
    assert "'test_tag' is an invalid version. No MR created." in caplog.text


@pytest.mark.parametrize(
    "tag, version",
    [("v1.2.3", "1.2.3"), ("2.0.0", "2.0.0"), ("3.1.15post1", "3.1.15.post1")],
)
@pytest.mark.asyncio
async def test_create_recipe_merge_request_valid_tag(caplog, mocker, tag, version):
    recipe_project = Project(name="foo-recipe", namespace="recipes")
    mock_find_project = mocker.patch("conda_bot.tasks.api.find_project")
    mock_find_project.return_value = recipe_project
    mocker.patch("conda_bot.util.get_archive_sha256")
    mocker.patch("conda_bot.util.create_recipe_mr")
    gl = FakeGitLab()
    project = Project(
        name="foo", namespace="test-group", path_with_namespace="test-group/foo"
    )
    with caplog.at_level(logging.INFO):
        await tasks.create_recipe_merge_request(gl, project, tag, 1)
    mock_find_project.assert_awaited()
    assert f"Creating MR to update {recipe_project.name} to {version}" in caplog.text
