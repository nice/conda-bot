import pytest
from gidgetlab import sansio
from conda_bot import pipelines
from gidgetlab_cli.models import Project
from .utils import FakeGitLab


@pytest.mark.asyncio
async def test_trigger_reverse_dependencies_build_on_success_status(mocker):
    mock = mocker.patch("conda_bot.pipelines.trigger_reverse_dependencies_pipelines")
    gl = FakeGitLab()
    project = Project(id=551, name="foo", namespace="test-group")
    data = {
        "object_attributes": {"status": "success", "stages": ["build", "release"]},
        "project": {
            "id": project.id,
            "name": project.name,
            "namespace": project.namespace,
        },
    }
    event = sansio.Event(data, event="Pipeline Hook")
    await pipelines.router.dispatch(event, gl)
    mock.assert_awaited_with(gl, project, True)


@pytest.mark.asyncio
async def test_trigger_reverse_dependencies_build_on_failed_status(mocker):
    mock = mocker.patch("conda_bot.pipelines.trigger_reverse_dependencies_pipelines")
    gl = FakeGitLab()
    name = "foo"
    namespace = "test-group"
    data = {
        "object_attributes": {"status": "failed"},
        "project": {"id": 551, "name": name, "namespace": namespace},
    }
    event = sansio.Event(data, event="Pipeline Hook")
    await pipelines.router.dispatch(event, gl)
    mock.assert_not_awaited()


@pytest.mark.asyncio
async def test_trigger_reverse_dependencies_build_on_build_stage_only(mocker):
    mock = mocker.patch("conda_bot.pipelines.trigger_reverse_dependencies_pipelines")
    gl = FakeGitLab()
    name = "foo"
    namespace = "test-group"
    data = {
        "object_attributes": {"status": "success", "stages": ["build"]},
        "project": {"id": 551, "name": name, "namespace": namespace},
    }
    event = sansio.Event(data, event="Pipeline Hook")
    await pipelines.router.dispatch(event, gl)
    mock.assert_not_awaited()
