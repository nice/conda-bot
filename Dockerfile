FROM registry.esss.lu.se/ics-docker/miniconda:4.8.4

USER 0

# Add csi user
RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -m -r -g csi -u 1000 csi

WORKDIR /app

COPY environment.yml /app/environment.yml
RUN conda env create -n conda-bot -f /app/environment.yml \
  && conda clean -a -y \
  && chown -R csi:csi /opt/conda/envs/conda-bot

COPY --chown=csi:csi . /app/

# Set PYTHONUNBUFFERED to 1 to force the stdout
# and stderr streams to be unbuffered
ENV PATH=/opt/conda/envs/conda-bot/bin:$PATH \
    PYTHONUNBUFFERED=1

USER 1000

# Running uvicorn is for testing
# For production, run using Gunicorn using the uvicorn worker class
# Use one or two workers per-CPU core
# For example:
# gunicorn -w 4 -k uvicorn.workers.UvicornWorker --log-level warning conda_bot.app:app
CMD ["uvicorn", "--host", "0.0.0.0", "--port", "8000", "conda_bot.app:app"]
