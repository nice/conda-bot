import gidgetlab.routing
from typing import Any
from gidgetlab.abc import GitLabAPI
from gidgetlab.sansio import Event
from gidgetlab_cli.models import Project
from .tasks import create_recipe_merge_request
from .settings import EXCLUDE_RECIPE_MR_PROJECTS

router = gidgetlab.routing.Router()


@router.register("Tag Push Hook")
async def create_recipe_mr(
    event: Event, gl: GitLabAPI, *args: Any, **kwargs: Any
) -> None:
    """Create a merge request for the recipe corresponding to the project"""
    if event.data["after"] == "0000000000000000000000000000000000000000":
        # Tag deleted - nothing to do
        return
    project = Project(**event.data["project"])
    if project.path_with_namespace in list(EXCLUDE_RECIPE_MR_PROJECTS):
        return
    tag = event.data["ref"].replace("refs/tags/", "")
    await create_recipe_merge_request(gl, project, tag, event.data["user_id"])
