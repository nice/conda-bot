import logging
from packaging.version import Version, InvalidVersion
from gidgetlab.httpx import GitLabAPI
from gidgetlab_cli.models import Project
from gidgetlab_cli import api
from . import util
from .settings import (
    CONDA_CHANNEL,
    CONDA_PLATFORM,
    RECIPE_GROUP_ID,
    RECIPE_PINNING_PROJECT_ID,
)

logger = logging.getLogger(__name__)


async def trigger_reverse_dependencies_pipelines(
    gl: GitLabAPI, project: Project, update_pinning: bool = True
) -> None:
    # WARNING! packages name are always in lower case
    package_name = project.name.replace("-recipe", "").lower()
    reverse_dependencies = util.get_direct_reverse_dependencies(
        package_name, str(CONDA_CHANNEL), CONDA_PLATFORM
    )
    if update_pinning:
        logger.info(f"Update pinning for {project.name}")
        await util.update_pinning(gl, project.id, RECIPE_PINNING_PROJECT_ID)
    if not reverse_dependencies:
        logger.info(f"{package_name} has no reverse dependency. Nothing to do.")
        return
    projects = [
        await api.find_project(gl, f"{name}-recipe", project.base_namespace)
        for name in reverse_dependencies
    ]
    # Remove recipes not found
    recipes = [p for p in projects if p is not None]
    if recipes:
        logger.info(
            f"Trigger pipeline for {package_name} reverse dependencies: {' '.join([r.name for r in recipes])}"
        )
        await util.trigger_pipelines(gl, recipes)
    else:
        logger.info(
            f"Recipes not found under {project.base_namespace} for {reverse_dependencies}"
        )


async def create_recipe_merge_request(
    gl: GitLabAPI, project: Project, tag: str, user_id: int
) -> None:
    logger.info(f"Tag {tag} pushed to {project.path_with_namespace} by user {user_id}")
    try:
        version = Version(tag)
    except InvalidVersion:
        logger.info(f"Tag '{tag}' is an invalid version. No MR created.")
        return
    recipe_name = f"{project.name}-recipe"
    recipe_project = await api.find_project(gl, recipe_name, RECIPE_GROUP_ID)
    if recipe_project is None:
        logger.info(f"Recipe {recipe_name} not found under {project.namespace}")
        return
    archive_sha256 = await util.get_archive_sha256(project, tag)
    logger.info(f"Creating MR to update {recipe_project.name} to {version.public}")
    await util.create_recipe_mr(
        gl, recipe_project.id, version.public, archive_sha256, user_id
    )
