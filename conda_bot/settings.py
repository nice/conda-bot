from starlette.config import Config
from starlette.datastructures import Secret, URL, CommaSeparatedStrings

config = Config(".env")

GL_URL = config("GL_URL", cast=URL, default="https://gitlab.esss.lu.se")
# The personal access token to use the GitLab API
GL_ACCESS_TOKEN = config("GL_ACCESS_TOKEN", cast=Secret, default="secret-token")
# The secret used when creating the webhook
# As the default is None, we can't cast to Secret
GL_SECRET = config("GL_SECRET", cast=str, default=None)

CONDA_CHANNEL = config(
    "CONDA_CHANNEL",
    cast=URL,
    default="https://artifactory.esss.lu.se/artifactory/api/conda/conda-e3",
)
CONDA_PLATFORM = "linux-64"

# Group id where to look for recipes when receiving a tag push event
RECIPE_GROUP_ID = config("RECIPE_GROUP_ID", cast=int, default="214")
# Do NOT try to create a MR for the following projects on tag push event
# (list of path_with_namespace)
EXCLUDE_RECIPE_MR_PROJECTS = config(
    "EXCLUDE_RECIPE_MR_PROJECTS", cast=CommaSeparatedStrings, default=""
)

# Project id of the repository with the global conda_build_config.yaml
# e3-recipes/e3-pinning by default
RECIPE_PINNING_PROJECT_ID = config("RECIPE_PINNING_PROJECT_ID", cast=int, default=1399)

# Sentry Data Source Name
# Leave it empty to disable it
SENTRY_DSN = config("SENTRY_DSN", cast=str, default="")
